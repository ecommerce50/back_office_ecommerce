const tbody = document.getElementById("table")
const infos = document.getElementById("details")
var totalPrice = document.getElementById("total")
var baskets


//récupération de la liste des paniers

function getBaskets() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:3333/baskets");
    xhr.readyState
    xhr.onload = () => {
        baskets = JSON.parse(xhr.responseText);
        updateList(baskets)
    }
    xhr.send();
}

getBaskets()

//mise à jour de la vue avec creation de la table 

function updateList(baskets) {
    for (i = 0; i < baskets.length; i++) {
        let bask = baskets[i]
        let tr = document.createElement("tr")
        let td1 = document.createElement("td")
        let id = baskets[i].id
        let td2 = document.createElement("td")
        let total = baskets[i].total
        let td3 = document.createElement("td")
        let date = baskets[i].date
        let td4 = document.createElement("button")
        td4.className = "btn btn-outline-secondary mt-1"
        td4.innerText = "Infos"
        td4.onclick = () => updateInfos(bask)
        td1.append(id)
        td2.append(total)
        td3.append(date)
        tr.append(td1, td2, td3, td4)
        tbody.appendChild(tr)
    }
}

// Affichage des infos du panier sélectionné 

function updateInfos(bask){
    while (infos.firstChild) {
        infos.removeChild(infos.firstChild);
      }

    bask.items.forEach(prods => {
        let card = document.createElement("div")
        card.className = "border border-secondary p-3 mt-2"
        let contentCard = document.createElement("div")
        contentCard.className = "d-flex justify-content-between"
        let h2 = document.createElement("h2")
        h2.innerText = prods.designation
        let qqt = document.createElement("h3")
        qqt.innerText = "Quantitée " + prods.quantity
        let price = document.createElement("h3")
        price.innerText = prods.price + "€"
        contentCard.append(qqt,price)
        card.append(h2,contentCard)
        infos.append(card)
    },
    calculPrice(bask)
);
}

//calcul du prix total 

function calculPrice(bask){
    var total = 0
    bask.items.forEach(element => {
         pItem = element.quantity * element.price
         console.log(pItem);
         total += pItem
    });
    totalPrice.innerText = total
}


