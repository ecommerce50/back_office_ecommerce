const table = document.getElementById("table")
var titre = document.getElementById("titre")
var btn = document.getElementById("btn")
var clearBtn = document.getElementById("clearbtn")
var updateBtn = document.getElementById("updatebtn")

titre.innerText = "Ajout de produit"
btn.innerText = "Ajouter"
//récupération de la liste des produits

function getProducts() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:3333/products");
    xhr.readyState
    xhr.onload = () => {
        products = JSON.parse(xhr.responseText);
        updateList(products)
    }
    xhr.send();
}
getProducts()

//chargement dde la liste des produits
function updateList(products) {
    while (table.firstChild) {
        table.removeChild(table.firstChild);
      }
    for (i = 0; i < products.length; i++) {
        let prod = products[i]
        let iProd = i
        let tr = document.createElement("tr")
        let td1 = document.createElement("td")
        let id = prod._id
        let td2 = document.createElement("td")
        let nom = prod.designation
        let td3 = document.createElement("td")
        let price = prod.price
        let td4 = document.createElement("td")
        let stock = prod.quantity
        let td5 = document.createElement("button")
        td5.className = "btn btn-outline-secondary mt-1"
        td5.innerText = "Modifier"
        td5.onclick = () => updateProd(prod,iProd)
        td1.append(id)
        td2.append(nom)
        td3.append(price)
        td4.append(stock)
        tr.append(td1, td2, td3, td4,td5)
        table.appendChild(tr)
    }
}

//gestion du boutton
btn.addEventListener("click", (e)=> {
    e.preventDefault()
    addProduct()
})

function addProduct() {
    let nom = document.getElementById("nom").value
    let prix = document.getElementById("prix").value
    let stock = document.getElementById("stock").value
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:3333/newProd");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    xhr.readyState
    xhr.onload = () => {
        products = JSON.parse(xhr.responseText);
        updateList(products)
    }
    xhr.send(`designation=${nom}&price=${prix}&quantity=${stock}`);
}

//gestion du changement de formulaire
function updateProd(prod,iProd){
    titre.innerText = "Modifier un produit"
    btn.style.display = "none"
    clearBtn.style.display = "inline-block"
    updateBtn.style.display = "inline-block"
    let nom = document.getElementById("nom")
    let prix = document.getElementById("prix")
    let stock = document.getElementById("stock")
    nom.value = prod.designation
    prix.value = prod.price
    stock.value = prod.quantity
    clearBtn.onclick = (e) => {
        e.preventDefault()
        console.log("ok");
        titre.innerText = "Ajout de produit"
        updateBtn.style.display = "none"
        btn.style.display = "inline-block"
        clearBtn.style.display = "none"
        nom.value = "Nom du produit"
        prix.value = "Prix"
        stock.value = "Stock"
    }
    updateBtn.onclick = (e) => {
        e.preventDefault()
        prod.designation = nom.value
        prod.price = prix.value
        prod.quantity = stock.value
        sendUpdate(iProd)
    }
}


function sendUpdate(iProd) {
    let xhr = new XMLHttpRequest();
    xhr.open("PUT", `http://localhost:3333/product/${iProd}`);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    xhr.readyState
    xhr.onload = () => {
        products = xhr.responseText;
        getProducts()
    }
    xhr.send(`designation=${nom.value}&price=${prix.value}&quantity=${stock.value}`);
}

   
